﻿//
//  Program.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Hackathon.ServiceInterface;
using ServiceStack;

namespace Hackathon.SelfHost
{
	//Define the Web Services AppHost
	public class AppHost : AppSelfHostBase
	{
		public AppHost ()
			: base ("HttpListener Self-Host", typeof(UsersService).Assembly)
		{
		}

		public override void Configure (Funq.Container container)
		{
		}
	}

	public static class Program
	{
		//Run it!
		static void Main (string[] args)
		{
			var listeningOn = args.Length == 0 ? "http://*:8989/" : args [0];
			var appHost = new AppHost ()
				.Init ()
				.Start (listeningOn);
			
			Console.WriteLine ("AppHost Created at {0}, listening on {1}", 
				DateTime.Now, listeningOn);
			
			Console.ReadKey ();
		}
	}
}

