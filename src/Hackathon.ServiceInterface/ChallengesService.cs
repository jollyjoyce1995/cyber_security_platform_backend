﻿//
//  ChallengesService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;
using Hackathon.ServiceModel.Types;
using ServiceStack;
using Hackathon.ServiceModel;
using ServiceStack.OrmLite;
using System.Net;
using System.Data;
using Hackathon.ServiceInterface.Points;
using System.Security.Cryptography;

namespace Hackathon.ServiceInterface
{
	public class ChallengesService : Service
	{
		/// <summary>
		/// Helper method to fetch a certain challenge from the database.
		/// Throws the appropriate HttpError Exceptions, in case of an error.
		/// </summary>
		/// <returns>The challenge from the database</returns>
		/// <param name="id">Id of the challenge</param>
		/// <param name="onlyIfAccesible">If set to <c>true</c>, a validation is done if the challenge is accesible to the logged in user (in an event the user is registered for and in a challengeset which is currently acitve)</param>
		private Challenge GetChallengeById (int id, bool onlyIfAccesible = false)
		{
			return GetChallengeById (this, id, onlyIfAccesible);
		}

		/// <summary>
		/// Helper method to fetch a certain challenge from the database.
		/// Throws the appropriate HttpError Exceptions, in case of an error.
		/// </summary>
		/// <returns>The challenge from the database</returns>
		/// <param name="service">The service which is requesting the data</param>
		/// <param name="id">Id of the challenge</param>
		/// <param name="onlyIfAccesible">If set to <c>true</c>, a validation is done if the challenge is accesible to the logged in user (in an event the user is registered for and in a challengeset which is currently acitve)</param>
		internal static Challenge GetChallengeById (Service service, int id, bool onlyIfAccesible)
		{
			if (onlyIfAccesible && !IsChallengeAvailableAndActive (service, id))
				throw HttpError.Forbidden ("This challenge is not available for you.");
			var challenges = service.Db.Select<Challenge> ().Where (c => c.Id == id);
			if (!challenges.Any ())
				throw HttpError.NotFound ("Challenge not found");
			if (challenges.Count () > 1)
				throw HttpError.Conflict ("More than one challenge with this ID found.");
			Challenge challenge = challenges.First ();
			return challenge;
		}



		internal static void LogChallengeAccess (IDbConnection Db, int challengeId, int userId, AccessType accessType)
		{
			var previousAccesses = Db.Select<ChallengeAccess> ().Where (c => (c.ChallengeId == challengeId && c.UserId == userId && c.Type == accessType));
			if (!previousAccesses.Any ()) {
				// Description has not been accessed before. We need to log the access
				Db.Insert<ChallengeAccess> (new ChallengeAccess () {
					UserId = userId,
					ChallengeId = challengeId,
					Type = accessType,
					AccessTime = DateTime.Now
				});
			}
		}

		internal static void LogHintAccess (IDbConnection Db, int challengeId, int userId, int hintNumber)
		{
			var previousAccesses = Db.Select<HintAccess> ().Where (c => (c.ChallengeId == challengeId && c.UserId == userId && c.HintId == hintNumber));
			if (!previousAccesses.Any ()) {
				// There has not been an access before. We need to log the access
				Db.Insert<HintAccess> (new HintAccess () {
					UserId = userId,
					ChallengeId = challengeId,
					HintId = hintNumber,
					AccessTime = DateTime.Now
				});
			}
		}

		private static bool IsChallengeAvailableAndActive (Service service, int challengeId)
		{
			using (var usersService = service.ResolveService<UsersService> ()) {
				//Get all events for the user
				var userEvents = usersService.Get (new GetLoggedInUserEvents ());
				using (var eventsService = service.ResolveService<EventsService> ()) {
					using (var challengeSetsService = service.ResolveService<ChallengeSetsService> ()) {
						foreach (int eventId in userEvents) {
							//Get all ChallengeSets in the current event
							var challengesets = eventsService.Get (new GetEvent {
								Id = eventId,
								OnlyCurrentAndPast = true
							}).ChallengeSets.Select (c => c.ChallengeSetId);
							foreach (int challengeSetId in challengesets) {
								//Get all challenges in the current ChallengeSet
								var challengeIds = challengeSetsService.Get (new GetChallengeSet{ Id = challengeSetId }).Challenges;
								//Is the requested challenge part of the current challengeset?
								if (challengeIds != null && challengeIds.Contains (challengeId))
									return true;
							}
						}
					}
				}
			}
			//If we reach this point, the challenge is not available or inactive
			return false;
		}

		[Authenticate]
		public FindChallengeResponse Get (FindChallenges request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			// We need to validate, if the requested challenge is contained in some event the user has access to
			return GetChallengeById (request.Id, true).ConvertTo<FindChallengeResponse> ();
		}

		[Authenticate]
		public GetChallengeDescriptionResponse Get (GetChallengeDescription request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			GetUserResponse loggedInUser;
			using (var usersService = ResolveService<UsersService> ()) {
				loggedInUser = usersService.Get (new GetLoggedInUser ());
			}

			// We need to validate, if the requested challenge is contained in some event the user has access to
			Challenge challenge = GetChallengeById (request.Id, true);

			GetChallengeDescriptionResponse ret = challenge.ConvertTo<GetChallengeDescriptionResponse> ();

			//Check if we have to replace a user token
			Match userTokenMatch = Regex.Match (ret.Description, @"{{{UserToken-([a-z]+)}}}");
			if (userTokenMatch.Success) {
				string userTokenIdentifier = userTokenMatch.Groups [1].Value;
				HMAC hmac = TryResolve<HMAC> ();
				if (hmac == null)
					throw HttpError.NotFound ("Not possible to calculate a user token");
				string toMac = String.Format ("{0}{1}", userTokenIdentifier, loggedInUser.UserName.Trim ());
				string userToken = Convert.ToBase64String (hmac.ComputeHash (UTF8Encoding.UTF8.GetBytes (toMac)));
				userToken = userToken.ReplaceAll ("/", "");
				userToken = userToken.Replace ("+", "");
				userToken = userToken.Replace ("=", "");
				ret.Description = ret.Description.Replace (userTokenMatch.Groups [0].Value, userToken);
			}

			//log the access to the description
			LogChallengeAccess (Db, request.Id, loggedInUserId, AccessType.Description);

			return ret;
		}

		[Authenticate]
		public void Post (PostSolution request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			//Fetch the challenge from the database
			Challenge challenge = GetChallengeById (request.Id, true);

			//Check if the user has already solved this challenge
			ChallengeSolution previousSolution = Db.Select<ChallengeSolution> ().Where (s => (s.UserId == loggedInUserId && s.ChallengeId == challenge.Id)).FirstOrDefault ();
			if (previousSolution != null && previousSolution.Solved)
				//User has already solved this solution. Return an error
				throw HttpError.Conflict ("You have already solved this challenge.");

			if (request.Solution == challenge.Solution) {
				//correct solution
				if (previousSolution == null)
					Db.Insert<ChallengeSolution> (new ChallengeSolution {
						ChallengeId = challenge.Id,
						UserId = loggedInUserId,
						Solved = true,
						SolutionTime = DateTime.Now,
					});
				else {
					previousSolution.Solved = true;
					previousSolution.SolutionTime = DateTime.Now;
					Db.Update (previousSolution);
				}

				// Calculate the points and store in in the database
				// Maybe we should caluclate the points via the point route instead of doing it ourselves??
				IPoints pointCalculator = TryResolve<IPoints> ();
				if (pointCalculator != null) {  //If there is no point calculator registered right now, we silently ignore this error, since the points can be calulated later on, too
					int points = pointCalculator.GetPoints (this, loggedInUserId, request.Id);
					Db.UpdateNonDefaults<ChallengeSolution> (new ChallengeSolution{ Points = points }, c => c.ChallengeId == challenge.Id && c.UserId == loggedInUserId);
				}
			} else {
				//bad solution
				if (previousSolution == null) {
					Db.Insert (new ChallengeSolution {	
						ChallengeId = challenge.Id,
						UserId = loggedInUserId,
						Solved = false,
						WrongEntries = 1
					});
				} else {
					previousSolution.WrongEntries++;
					Db.Update (previousSolution);
				}

				//Tell the client that the solution was bad
				throw new HttpError (HttpStatusCode.BadRequest);
			}
		}

		[Authenticate]
		public List<int> Get (GetChallengeHints request)
		{
			//Check if the challenge exits
			GetChallengeById (request.Id, true);

			return Db.Select<int> (Db.From<Hint> ()
				.Where (h => h.ChallengeId == request.Id)
				.Select (h => h.Id)
			);
		}

		[Authenticate]
		public GetChallengeHintResponse Get (GetChallengeHint request)
		{
			var session = this.GetSession ();
			int userId = Int32.Parse (session.UserAuthId);

			Challenge challenge = GetChallengeById (request.Id, true);
			var hints = Db.Select<Hint> (h => h.Id == request.HintNumber && h.ChallengeId == challenge.Id);
			if (!hints.Any () || hints.Count > 1)
				throw HttpError.NotFound ("No such hint available");
			
			LogHintAccess (Db, request.Id, userId, request.HintNumber);

			return new GetChallengeHintResponse () {
				HintNumber = request.HintNumber,
				Text = hints.First ().Text
			};
		}

		[Authenticate]
		public PointResponse Get (GetHintPointReduction request)
		{
			int loggedInUserId;
			if (!Int32.TryParse (GetSession ().UserAuthId, out loggedInUserId))
				throw HttpError.Forbidden ("No valid authenticated user found.");

			DateTime accessTime = DateTime.MinValue;
			using (var usersService = ResolveService<UsersService> ()) {
				var userChallenge = usersService.Get (new GetUserChallenge {
					ChallengeId = request.Id,
					UserId = loggedInUserId
				});

				if (!userChallenge.AccessDate.IsNullOrEmpty ())
					accessTime = userChallenge.AccessDate.FromEcmaScriptString ();
			}
			//Get the already used hints
			List<int> accessedHintIds = Db.Select<HintAccess> (h => ((h.ChallengeId == request.Id) && (h.UserId == loggedInUserId))).Select (h => h.HintId).ToList ();

			// Calculate the points without the hint
			IPoints pointCalculator = TryResolve<IPoints> ();
			int pointsWithoutHint = pointCalculator.GetPoints (this, loggedInUserId, request.Id, DateTime.Now, accessTime, accessedHintIds);

			// Calculate the points with the hint
			accessedHintIds.Add (request.HintNumber);
			int pointsWithHint = pointCalculator.GetPoints (this, loggedInUserId, request.Id, DateTime.Now, accessTime, accessedHintIds);

			return new PointResponse (){ Points = (pointsWithoutHint - pointsWithHint) };
		}
	
	}
}

